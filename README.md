# Sourceeasy Developer Box 


## Pre-Install 


- Install [Vagrant](https://www.vagrantup.com/downloads.html) and [Virtual Box](https://www.virtualbox.org/wiki/Downloads) for your respective platforms. 
- Clone [https://bitbucket.org/sourceasy/sunshine/](https://bitbucket.org/sourceasy/sunshine/)
- Read [README.md](https://bitbucket.org/sourceasy/sunshine)
- Run "run_first.sh". This will install the ubuntu/trusty64 version v20160122.0.0 to your vagrant configuration.
- Open and config the ```config.yml```. 


## Configuaration

You'll can update ```config.yml``` as per your details. Only things I would advice against is  **ip** and **box** unless you know what you are doing. 
  

## Setup 

- If this is the first time you installing the box, run ```vagrant up``` else try ```vagrant up --provision```.
- Setup won't ideally break, but if it does please run ```vagrant halt && vagrant up --provision``` and in extreme cases ```vagrant destroy && vagrant up```

### Vangrant Errors

- Please run ```VBoxManage list runningvms``` to see list of VM's running
- If the above command returns empty, we are good to go, otherwise please shutdown the non required VM's by ```VBoxManage controlvm NAMEOFVM poweroff```



## Notes 

Setup wil install

- Vim
- Php
- Nginx
- Postgress
- Mongodb
- Memcached
- Nodejs
- Composer
- Sqlite
- SupervisorD

A new user ```vagrant``` with databse ```vagrant``` is create for you, you can choose to use it if you want or setup your own.


## Goodies 

/vagrantbox/goodies

- nginx
- env example file

## Usage

> Remember before first use to halt the vm before sshing ```vagrant halt && vagrant up && vagrant ssh```


- Type ```vagrant ssh``` and you are in business. You should be able to see all your files etc inside the ```Code``` folder. 
- Your server will be avilable at ```192.168.9.108```.
- If it says forbidden, just correct the path in the ```/etc/nginx/sites-available/default```


Rest you already know. 

![Final Screen](http://i.imgur.com/VHBXaSC.png)

