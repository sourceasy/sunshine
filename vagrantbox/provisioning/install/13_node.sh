#!/usr/bin/env bash

if command -v node >/dev/null ; then
	echo -e ">>> Node already installed"
else

	echo -e ">>> Installng Node, Bower and Gulp"
	curl -sL https://deb.nodesource.com/setup_0.10 | sudo -E bash -
	sudo apt-get install -y nodejs
	sudo npm install -g gulp
	sudo npm install -g bower
	sudo npm install -g grunt-cli
	echo -e "\u2705  >>> Node, Gulp ,Grunt and Bower Installed "
fi