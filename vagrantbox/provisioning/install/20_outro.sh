#!/usr/bin/env bash


echo -e "\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d"
echo "List of software installed"
echo "+++ Git"
echo "+++ PHP"
echo "+++ Nginx"
echo "+++ NodeJS"
echo "++++++ Gulp"
echo "++++++ Grunt"
echo "++++++ Bower"
echo "+++ Composer"
echo "+++ PostgresSQL"
echo "+++ Mongodb"
echo "+++ Memcached"
echo "+++ Redis"
echo "+++ SQLite Server"
echo "+++ Supervisord"
echo -e "\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d"
echo -e "\u2705  >>> Install complete, 'vagrant ssh'"

sleep 5
sudo shutdown -r now