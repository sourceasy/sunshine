#!/usr/bin/env bash

if command -v memcached >/dev/null ; then
    echo -e ">>> Memcached already installed"
else
	echo ">>> Installing Memcached"
	sudo apt-get install -qq memcached
fi