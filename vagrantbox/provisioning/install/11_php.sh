#!/usr/bin/env bash

SERVER_TIMEZONE=$1
GIT_NAME=$2
GIT_EMAIL=$3
SERVER_IP = $4


if command -v php >/dev/null ; then
	echo -e ">>> PHP 5.6 already installed"   
else
echo -e ">>> Installing PHP 5.6"

## sudo add-apt-repository -y ppa:ondrej/php5-5.6 > /dev/null
sudo apt-get update  -y --force-yes -qq > /dev/null
sudo apt-get upgrade -y --force-yes -qq > /dev/null
sudo apt-get install -qq php5-cli php5-fpm php5-mysql php5-pgsql php5-sqlite php5-curl php5-gd php5-gmp php5-mcrypt php5-memcached php5-imagick php5-intl php5-xdebug > /dev/null
echo -e "\u2705  >>> Installed php5, php5-fpm, php5-pgsql, php-curl, php5-gd php5-gmp php5-mcrypt php5-memcached php5-imagick php5-intl php5-xdebug"

echo -e ">>> Set run-as user for PHP5-FPM processes to user/group vagrant, to avoid permission errors from apps writing to files"

sudo sed -i "s/listen =.*/listen = 127.0.0.1:9000/" /etc/php5/fpm/pool.d/www.conf
sudo sed -i "s/;listen.allowed_clients/listen.allowed_clients/" /etc/php5/fpm/pool.d/www.conf
sudo sed -i "s/user = www-data/user = vagrant/" /etc/php5/fpm/pool.d/www.conf
sudo sed -i "s/group = www-data/group = vagrant/" /etc/php5/fpm/pool.d/www.conf
sudo sed -i "s/listen\.owner.*/listen.owner = vagrant/" /etc/php5/fpm/pool.d/www.conf
sudo sed -i "s/listen\.group.*/listen.group = vagrant/" /etc/php5/fpm/pool.d/www.conf
sudo sed -i "s/listen\.mode.*/listen.mode = 0666/" /etc/php5/fpm/pool.d/www.conf

echo -e ">>> PHP Error Reporting Config"
sudo sed -i "s/error_reporting = .*/error_reporting = E_ALL/" /etc/php5/fpm/php.ini
sudo sed -i "s/display_errors = .*/display_errors = On/" /etc/php5/fpm/php.ini

echo -e ">>> PHP Date Timezone"
sudo sed -i "s/;date.timezone =.*/date.timezone = ${SERVER_TIMEZONE/\//\\/}/" /etc/php5/fpm/php.ini
sudo sed -i "s/;date.timezone =.*/date.timezone = ${SERVER_TIMEZONE/\//\\/}/" /etc/php5/cli/php.ini

echo -e ">>> Installing Mailparse for manipulation with emails..."
pecl install mailparse
echo "extension=mailparse.so" > /etc/php5/mods-available/mailparse.ini
ln -s /etc/php5/mods-available/mailparse.ini /etc/php5/cli/conf.d/20-mailparse.ini

echo -e ">>> Restarting php5-fpm"
sudo service php5-fpm restart

echo -e ">>> PHP Installed with all the sidekicks"
echo -e ">>> PHP5-FPM added and configured"
echo -e ">>> PHP5 Reporting setup and configured"
fi



