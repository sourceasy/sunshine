#!/usr/bin/env bash

if command -v composer >/dev/null ; then
	echo -e ">>> Composer already installed"
else
	echo -e ">>> Installing Composer"
	curl -sS https://getcomposer.org/installer | php
	mv composer.phar /usr/local/bin/composer
fi