#!/usr/bin/env bash
if command -v nginx >/dev/null ; then
	echo -e ">>> Nginx already installed"   
else

echo -e ">>> Installing nginx"
sudo apt-get install -qq nginx

# Turn off sendfile to be more compatible with Windows, which can't use NFS

echo -e ">>> Set run-as user for PHP5-FPM processes to user/group 'vagrant'"

sed -i 's/sendfile on;/sendfile off;/' /etc/nginx/nginx.conf
sed -i "s/user www-data;/user vagrant;/" /etc/nginx/nginx.conf
sed -i "s/# server_names_hash_bucket_size.*/server_names_hash_bucket_size 64;/" /etc/nginx/nginx.conf
sudo usermod -a -G www-data vagrant

echo -e ">>> PHP-FPM Config for Nginx"
sed -i "s/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/" /etc/php5/fpm/php.ini
sudo service php5-fpm restart
echo -e ">>> PHP-FPM Config for Nginx"
sudo service nginx restart

echo ">>> Setting up the default nginx Config"
echo "+++ box IP is 192.168.9.109 ip as sebox with ~/Code/ "
echo "+++ 192.168.9.108 api3.dev"
echo "+++ 192.168.9.108 platform.dev"

wget http://bit.ly/1RQ52ft 
sudo rm /etc/nginx/sites-available/default
sudo mv 1RQ52ft /etc/nginx/sites-available/default
sudo rm /etc/nginx/sites-enabled/default
sudo ln -s /etc/nginx/sites-available/default /etc/nginx/sites-enabled/default
sudo service nginx restart

echo -e "\u2705  >>> Nginx is setup and configured"

fi