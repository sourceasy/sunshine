#!/usr/bin/env bash

	# Set some variables
	POSTGRE_VERSION=9.4
	echo ">>> Installing PostgreSQL"
	[[ -z "$1" ]] && { echo "!!! PostgreSQL root password not set. Check the Vagrant file."; exit 1; }
	sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ trusty-pgdg main" >> /etc/apt/sources.list.d/pgdg.list'
	wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | \
    sudo apt-key add -
	sudo apt-get update
	sudo apt-get install postgresql-9.4 postgresql-contrib-9.4 -y --force-yes -qq

	# Configure PostgreSQL
	# Listen for localhost connections
	sudo sed -i "s/#listen_addresses = 'localhost'/listen_addresses = '*'/g" /etc/postgresql/$POSTGRE_VERSION/main/postgresql.conf
	echo "host    all             all             0.0.0.0/0               md5" | sudo tee -a /etc/postgresql/$POSTGRE_VERSION/main/pg_hba.conf

	sudo service postgresql start

	
	# GRANT ALL PRIVILEGES ON DATABASE server to local;

	sudo -u postgres psql -c "CREATE USER vagrant WITH PASSWORD 'pass'";
	sudo -u postgres psql -c "CREATE DATABASE vagrant";
	sudo -u postgres psql -c "GRANT ALL PRIVILEGES ON DATABASE vagrant1 to vagrant;";

	# Create new superuser "vagrant"
	# sudo -u postgres createuser -s vagrant
	# Create new user "root" w/ defined password
	# Not a superuser, just tied to new db "vagrant"
	# sudo -u postgres psql -c "CREATE ROLE root LOGIN UNENCRYPTED PASSWORD '$1' NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;"
	# Make sure changes are reflected by restarting
	sudo service postgresql restart