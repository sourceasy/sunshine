 #!/usr/bin/env bash


sudo locale-gen en_US -qq 
sudo update-locale LANG=en_US.UTF-8 LC_CTYPE=en_US.UTF-8
echo -e "\u2705  >>> Locale set to en_US"
