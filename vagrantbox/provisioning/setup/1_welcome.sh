 #!/usr/bin/env bash

# Setting args 
SERVER_TIMEZONE=$1
GIT_NAME=$2
GIT_EMAIL=$3
SERVER_IP=$4

echo -e "\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d"
echo -e "\u2b50  >>> Hello $GIT_NAME, initializing...give it sometime, may take upto 15 mins"
echo "Email: $GIT_EMAIL and IP: $SERVER_IP - If you don't see your email and ip address, press Crtl+C and restart"
echo -e "\u2b50  >>> FYI: Ubuntu Version Info"
cat /etc/lsb-release
echo -e "\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d\u254d"