 #!/usr/bin/env bash


SERVER_TIMEZONE=$1
GIT_NAME=$2
GIT_EMAIL=$3


ln -s /home/vagrant/project/dotfiles/git/gitattributes /home/vagrant/.gitattributes
ln -s /home/vagrant/project/dotfiles/git/gitconfig /home/vagrant/.gitconfig
ln -s /home/vagrant/project/dotfiles/git/gitignore /home/vagrant/.gitignore

echo -e "\u2705   >>> Git configured."