 #!/usr/bin/env bash

sudo rm	-f /home/vagrant/.bashrc
ln -s /home/vagrant/project/dotfiles/shell/bashrc /home/vagrant/.bashrc
source /home/vagrant/.bashrc  || { echo 'Something went wrong, need to source .file again.' ; exit 1; }
echo -e "\u2705   >>> Bash configured."