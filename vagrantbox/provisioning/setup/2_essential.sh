 #!/usr/bin/env bash

echo -e ">>> Installing Essential Packages"
perl -pi -e 's@^\s*(deb(\-src)?)\s+http://us.archive.*?\s+@\1 mirror://mirrors.ubuntu.com/mirrors.txt @g' /etc/apt/sources.list
echo ">>> Optimizing apt sources to select best mirrors (Around 5-10 mins)"

the_PHP_ppa=ondrej/php5-5.6
the_NG_ppa=nginx/stable
the_RED_ppa=rwky/redis



if ! grep -q "$the_PHP_ppa" /etc/apt/sources.list /etc/apt/sources.list.d/*; then
	echo "+++ Adding PHP ppa +++"
	sudo add-apt-repository -y ppa:ondrej/php5-5.6 > /dev/null
fi

if ! grep -q "$the_NG_ppa" /etc/apt/sources.list /etc/apt/sources.list.d/*; then
	echo "+++ Adding Nginx ppa +++"
	sudo add-apt-repository -y ppa:nginx/stable > /dev/null
fi

if ! grep -q "$the_RED_ppa" /etc/apt/sources.list /etc/apt/sources.list.d/*; then
	echo "+++ Adding Redis ppa +++"
	sudo apt-add-repository -y ppa:rwky/redis  > /dev/null
fi



echo "+++ Updating Ubuntu... +++"
sudo apt-get update  -y --force-yes -qq
echo "+++ Upgrading Ubuntu... +++"
sudo apt-get upgrade -y --force-yes -qq
echo "+++ Installing Wget.  +++"
sudo apt-get install -qq wget > /dev/null 2>&1
echo "+++ Installing CURL.  +++"
sudo apt-get install -qq curl > /dev/null 2>&1
echo "+++ Installing Unzip.  +++"
sudo apt-get install -qq unzip > /dev/null 2>&1
echo "+++ Installing Git.  +++"
sudo apt-get install -qq git-core > /dev/null 2>&1
echo "+++ Installing ack-grep.  +++"
sudo apt-get install -qq ack-grep  > /dev/null 2>&1
echo "+++ Installing software-properties-common.  +++"
sudo apt-get install -qq software-properties-common  > /dev/null 2>&1
echo "+++ Installing build-essential.  +++"
sudo apt-get install -qq build-essential > /dev/null 2>&1
echo "+++ Installing cachefilesd.  +++"
sudo apt-get install -qq cachefilesd  > /dev/null 2>&1
echo "+++ Installing python-software-properties.  +++"
sudo apt-get install -qq python-software-properties > /dev/null 2>&1
echo "+++ Installing language-pack-en.  +++"
sudo apt-get install -qq language-pack-en > /dev/null 2>&1