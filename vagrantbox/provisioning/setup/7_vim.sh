 #!/usr/bin/env bash

# Setting args 
SERVER_TIMEZONE=$1
GIT_NAME=$2
GIT_EMAIL=$3


# Create directories needed for some .vimrc settings

mkdir -p /home/vagrant/.vim/backup
mkdir -p /home/vagrant/.vim/swap
sudo chown -R vagrant:vagrant /home/vagrant/.vim

# Create directories needed for some .vimrc settings
ln -s /home/vagrant/project/dotfiles/vim/vim /home/vagrant/.vim
ln -s /home/vagrant/project/dotfiles/vim/vimrc /home/vagrant/.vimrc

echo -e "\u2705   >>> Vim Configured."