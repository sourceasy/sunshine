 #!/usr/bin/env bash


sudo ln -sf /usr/share/zoneinfo/$SERVER_TIMEZONE /etc/localtime
echo -e "\u2705  >>> Timezone to UTC"