 #!/usr/bin/env bash

# Setting args 
SERVER_TIMEZONE=$1
GIT_NAME=$2
GIT_EMAIL=$3
SERVER_IP=$4


echo ">>> setting up the dotfiles"
if [ -d /home/vagrant/project ] ; then

	echo ">>> Hold on, need to delete stuff."
	sudo rm -rf /home/vagrant/project
	sudo rm -rf /home/vagrant/.vim
	
	sudo rm	-f /home/vagrant/.gitattributes
	sudo rm -f /home/vagrant/.gitconfig
	sudo rm -f /home/vagrant/.gitignore
	sudo rm -f /home/vagrant/.vim
	sudo rm -f /home/vagrant/.vimrc
	sudo rm -f /home/vagrant/.bashrc

	sudo mkdir /home/vagrant/project
	git clone https://github.com/chiragchamoli/dotfiles.git /home/vagrant/project/dotfiles || { echo 'Error fetching the git repo after deletion.' ; exit 1; }
	
else 

	mkdir /home/vagrant/project
	echo ">>> pulling dotfiles from the repo"
	git clone https://github.com/chiragchamoli/dotfiles.git /home/vagrant/project/dotfiles || { echo 'Fresh error fetching the git repo' ; exit 1; }

fi
