# -*- mode: ruby -*-
# vi: set ft=ruby :
# Require YAML module


require 'yaml'
params = YAML::load_file("./config.yml")

Vagrant.configure(2) do |config|

  config.vm.box = params['box']

  bitbucket_username    = params['bitbucket_username']
  server_ip             = params['ip']
  srcFolder             = params['folder_local']
  destFolder            = params['folder_vm']
  public_folder         = destFolder
  server_cpus           = params['server_cpus']  # Cores
  server_memory         = "1024" # MB
  server_swap           = "1025" # Options: false | int (MB) - Guideline: Between one or two times the server_memory
  server_timezone       = "UTC"
  hostname              = params['hostname']
  # php
  php_timezone          = "UTC"    # http://php.net/manual/en/timezones.php
  php_version           = "5.6"
  # pgsql
  pgsql_root_password   = "pass"
  # mongo
  mongo_version         = "2.6"    # Options: 2.6 | 3.0
  mongo_enable_remote   = "true"  # remote access enabled when true
  git_name                  = params['git_name']
  git_email                  = params['git_email']

  #config.vm.box_url = "https://vagrantcloud.com/ubuntu/boxes/trusty64/versions/14.04/providers/virtualbox.box"
  config.vm.box_check_update = false
  config.vm.network "private_network", ip: server_ip
  config.vm.network "forwarded_port", guest: 80, host: 8080
  config.vm.hostname = hostname
  config.vm.synced_folder srcFolder, destFolder, id: "vagrant-root",
    owner: "vagrant",
    group: "www-data",
    mount_options: ["dmode=775,fmode=664"]

   # If using VirtualBox
  config.vm.provider :virtualbox do |vb|
    vb.name = hostname
    vb.customize ["modifyvm", :id, "--cpus", server_cpus]
    vb.customize ["modifyvm", :id, "--memory", server_memory]
    vb.customize ["guestproperty", "set", :id, "/VirtualBox/GuestAdd/VBoxService/--timesync-set-threshold", 10000]
  end

  # setup 

  config.vm.provision "shell", path: "./vagrantbox/provisioning/setup/1_welcome.sh", args: [server_timezone, git_name, git_email,server_ip]
  config.vm.provision "shell", path: "./vagrantbox/provisioning/setup/2_essential.sh", args: [server_timezone, git_name, git_email]
  config.vm.provision "shell", path: "./vagrantbox/provisioning/setup/3_timezone.sh", args: [server_timezone, git_name, git_email]
  config.vm.provision "shell", path: "./vagrantbox/provisioning/setup/4_locale.sh", args: [server_timezone, git_name, git_email]
  config.vm.provision "shell", path: "./vagrantbox/provisioning/setup/5_build.sh", args: [server_timezone, git_name, git_email]
  config.vm.provision "shell", path: "./vagrantbox/provisioning/setup/6_dot.sh", args: [server_timezone, git_name, git_email]
  config.vm.provision "shell", path: "./vagrantbox/provisioning/setup/7_vim.sh", args: [server_timezone, git_name, git_email]
  config.vm.provision "shell", path: "./vagrantbox/provisioning/setup/8_bash.sh", args: [server_timezone, git_name, git_email]
  config.vm.provision "shell", path: "./vagrantbox/provisioning/setup/9_git.sh", args: [server_timezone, git_name, git_email]
  config.vm.provision "shell", path: "./vagrantbox/provisioning/setup/10_handover.sh", args: [server_timezone, git_name, git_email]


  # install

  config.vm.provision "shell", path: "./vagrantbox/provisioning/install/11_php.sh", args: [server_timezone, git_name, git_email,server_ip]
  config.vm.provision "shell", path: "./vagrantbox/provisioning/install/12_nginx.sh", args: [server_timezone, git_name, git_email,server_ip]
  config.vm.provision "shell", path: "./vagrantbox/provisioning/install/13_node.sh", args: [server_timezone, git_name, git_email,server_ip]
  config.vm.provision "shell", path: "./vagrantbox/provisioning/install/14_composer.sh", args: [server_timezone, git_name, git_email,server_ip]
  config.vm.provision "shell", path: "./vagrantbox/provisioning/install/15_postgresql.sh", args: pgsql_root_password
  config.vm.provision "shell", path: "./vagrantbox/provisioning/install/16_mongodb.sh", args: [mongo_enable_remote, mongo_version]
  config.vm.provision "shell", path: "./vagrantbox/provisioning/install/17_memcached.sh", args: [server_timezone, git_name, git_email,server_ip]
  #!config.vm.provision "shell", path: "./vagrantbox/provisioning/install/18_redis.sh", args: [server_timezone, git_name, git_email,server_ip]
  config.vm.provision "shell", path: "./vagrantbox/provisioning/install/19_utils.sh", args: [server_timezone, git_name, git_email,server_ip]
  config.vm.provision "shell", path: "./vagrantbox/provisioning/install/20_outro.sh", args: [server_timezone, git_name, git_email,server_ip]
end
